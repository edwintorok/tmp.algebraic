(* OASIS_START *)
(* DO NOT EDIT (digest: d57749be8a05bd8dfef16e0a90982c18) *)

algebra - Symbolic math
=======================

See the file [INSTALL.txt](INSTALL.txt) for building and installation
instructions.

Copyright and license
---------------------

algebra is distributed under the terms of the Berkeley software distribution
license (3 clauses).

(* OASIS_STOP *)
