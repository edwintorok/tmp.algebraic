type 'a repr
type number
type t = number repr
include Convertible.String with type t := t
include Convertible.OfInt with type t := t
include Ops.Compare with type t := t
val (+) : t -> t -> t
val (~-) : t -> t
val (-) : t -> t -> t
val ( * ) : t -> t -> t
val ( ** ) : t -> t -> t
