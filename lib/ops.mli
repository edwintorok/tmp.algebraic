module type Compare = sig
  type t
  val _0 : t
  val _1 : t
  val compare : t -> t -> int
  val (<) : t -> t -> bool
  val (<=) : t -> t -> bool
  val (=) : t -> t -> bool
  val (>=) : t -> t -> bool
  val (>) : t -> t -> bool
  val (<>) : t -> t -> bool
  val sign : t -> int
  val min : t -> t -> t
  val max : t -> t -> t
  val abs : t -> t
end
