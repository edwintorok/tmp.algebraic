module CompareAux(Op: sig
    type t
    val _0 : t
    val compare : t -> t -> int
    val (<) : t -> t -> bool
    val (>) : t -> t -> bool
end) = struct
  open Op
  let sign x = compare x _0
  let min a b = if a < b then a else b
  let max a b = if a > b then a else b
end

module Compare(T : sig
    type t
    val of_int : int -> t
    val compare : t -> t -> int
  end) = struct
  module Op = struct
    type t = T.t
    let _0 = T.of_int 0
    let _1 = T.of_int 1
    let compare = T.compare
    let (<) a b = compare a b < 0
    let (<=) a b = compare a b <= 0
    let (=) a b = compare a b = 0
    let (>=) a b = compare a b >= 0
    let (>) a b = compare a b > 0
    let (<>) a b = compare a b <> 0
  end
  include Op
  include CompareAux(Op)
end

module type Trans = sig
  type 'a repr
  type 'a term
  val fwd: 'a repr -> 'a term
  val bwd: 'a term -> 'a repr
end

module MakeIntegral(I: module type of IntegralDomain)(X:Trans with type 'a repr = 'a I.repr) = struct
  type 'a repr = 'a X.term
  type number = I.number
  type t = number repr

  let bwd1 f a = f (X.bwd a)
  let bwd2 f a b = f (X.bwd a) (X.bwd b)
  let apply1 f a = X.fwd (f (X.bwd a))
  let apply2 f a b = X.fwd (f (X.bwd a) (X.bwd b))

  let _0 = X.fwd I._0
  let _1 = X.fwd I._1
  let of_int i = X.fwd (I.of_int i)
  let of_string s = X.fwd (I.of_string s)
  let to_string = bwd1 I.to_string

  let compare = bwd2 I.compare
  let (<) = bwd2 I.(<)
  let (<=) = bwd2 I.(<=)
  let (=) = bwd2 I.(=)
  let (>=) = bwd2 I.(>=)
  let (>) = bwd2 I.(>)
  let (<>) = bwd2 I.(<>)
  let (>) = bwd2 I.(<)
  let sign = bwd1 I.sign

  let min = apply2 I.min
  let max = apply2 I.max
  let abs = apply1 I.abs

  let (+) = apply2 I.(+)
  let (~-) = apply1 I.(~-)
  let (-) = apply2 I.(-)
  let ( * ) = apply2 I.( * )
  let ( ** ) = apply2 I.( ** )
end

module MakeField(F: module type of Field)(X:Trans with type 'a repr = 'a F.repr) = struct
  include MakeIntegral(F)(X)
  open F
  let (/) a b = X.fwd ((X.bwd a) / (X.bwd b))
end
