module MakeLazy(F : module type of Field) = struct
  type 'a repr = 'a F.repr Lazy.t
  type number = F.number
  type t = number repr

  let bwd1 f a = f (Lazy.force a)
  let bwd2 f a b = f (Lazy.force a) (Lazy.force b)
  let apply1 f a = lazy (f (Lazy.force a))
  let apply2 f a b = lazy (f (Lazy.force a) (Lazy.force b))

  let _0 = Lazy.from_val F._0
  let _1 = Lazy.from_val F._1
  let of_int i = lazy (F.of_int i)
  let of_string s = lazy (F.of_string s)
  let to_string = bwd1 F.to_string

  let compare = bwd2 F.compare
  let (<) = bwd2 F.(<)
  let (<=) = bwd2 F.(<=)
  let (=) = bwd2 F.(=)
  let (>=) = bwd2 F.(>=)
  let (>) = bwd2 F.(>)
  let (<>) = bwd2 F.(<>)
  let (>) = bwd2 F.(<)
  let sign = bwd1 F.sign

  let min = apply2 F.min
  let max = apply2 F.max
  let abs = apply1 F.abs

  let (+) = apply2 F.(+)
  let (~-) = apply1 F.(~-)
  let (-) = apply2 F.(-)
  let ( * ) = apply2 F.( * )
  let ( ** ) = apply2 F.( ** )
  let (/) = apply2 F.(/)
end

module Make(F: module type of Field) = struct
  module T = struct
    type pow = {
      base: t;
      pow: t;
    }

    type 'a repr = 'a F.repr
    type 'a term =
      | V : F.t -> number term
      | VPow : (F.t * F.t) -> number term
      | Unk : F.t -> number term

    type t = {
      exp : exp;
      computable: F.t Lazy.t;
    }
    and exp =
      | V of F.t
      | VPow of pow
      | Neg of t
      | Add of t list
      | Inv of t
      | Mul of t list
      | Sign of t
      | Abs of t

    let of_int i =
      let base = of_int i in V base, Lazy.from_val base

    let observe s = Lazy.force s.computable

    let add a b = F.add (Lazy.force )
    let eval_add lst = List.fold_left add _0 lst
    let add_exp lst = { exp = Add lst; computable = lazy (eval_add lst) }

    let add a b = match a.exp, b.exp with
      | V _ as v1, V _ as v2 -> add_exp (v1 :: v2)
      | Add l1, Add l2 -> Add (List.rev_append l1 l2)
      | Add l, V v | V v, Add l ->
        Add (v :: l)

    let compare a b = match a.exp, b.exp with
      | V v1, V v2 -> F.compare (Lazy.force a.computable) (Lazy.force b.computable)
      | _ -> sign (sub a b)

  end
  include Generic.Compare(T)
end
