open Num
let of_string = num_of_string
let to_string = string_of_num
let of_int = num_of_int
let _1 = of_int 1
module T = struct
  type t = num
  let _0 = of_int 0
  let compare  = compare_num
  let (<) = lt_num
  let (>) = gt_num
end
include T
include Generic.CompareAux(T)
let (<=) = le_num
let (=) = eq_num
let (>=) = ge_num
let (<>) a b = not (eq_num a b)

let (+) = add_num
let (-) = sub_num
let (~-) x = _0 - x
let abs = abs_num

let ( * ) = mult_num
let ( / ) = div_num
let ( ** ) = power_num

module Limit = struct
  let max = None
  let min = None
  let epsilon = None
end
