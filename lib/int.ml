type 'a repr = 'a
type number = int
let of_int (x:int) = x
let of_string = int_of_string
let to_string = string_of_int
module T = struct
  type t = int
  let _0 = 0
  let _1 = 1
  let compare (a:int) (b:int) = Pervasives.compare a b
  let (<) (a:int) (b:int) = a < b
  let (>) (a:int) (b:int) = a > b
end
include T
let (<=) (a:int) (b:int) = a <= b
let (=) (a:int) (b:int) = a = b
let (>=) (a:int) (b:int) = a >= b
let (<>) (a:int) (b:int) = a <> b
include Generic.CompareAux(T)

let (+) = Pervasives.(+)
let (~-) = Pervasives.(~-)
let abs = Pervasives.abs
let (-) = Pervasives.(-)

let ( * ) = Pervasives.( * )

let sq x = x

let rec ( ** ) a n =
  if n = 0 then 1
  else if n = 1 then a
  else
    let r = sq (a ** (n/2)) in
    if n mod 2 = 0 then r
      else r * a

module Limit = struct
  let max = Some max_int
  let min = Some min_int
  let epsilon = Some 1
end
