module type String = sig
  type t
  val of_string : string -> t
  val to_string : t -> string
end

module type OfInt = sig
  type t
  val of_int : int -> t
end

module type Float = sig
  type t
  val of_float : float -> t
  val approx_float : t -> float
  val approx_string_fix : int -> t -> string
  val approx_string_exp : int -> t -> string
end
