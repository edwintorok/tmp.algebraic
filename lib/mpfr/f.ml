let of_string = Mpfrf.of_string
let to_string = Mpfrf.to_string

let round = Mpfr.Near

module T = struct
  type t = Mpfrf.t
  let of_int i = Mpfrf.of_int i round
  let compare = Mpfrf.cmp
end
include Generic.Compare(T)
let (+) a b = Mpfrf.add a b round
let (-) a b = Mpfrf.sub a b round
let ( * ) a b = Mpfrf.mul a b round
let ( ** ) a b = Mpfrf.pow a b round
let ( / ) a b = Mpfrf.div a b round

let infinity = Mpfrf.of_float infinity round
let neg_infinity = Mpfrf.of_float neg_infinity round
